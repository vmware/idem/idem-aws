import unittest.mock

import pytest


key_list_1 = {
    "result": True,
    "ret": [
        {"access_key_id": "KEYID1", "status": "Active", "user_name": "mock_test_user"}
    ],
    "comment": [],
    "ref": "exec.aws.iam.access_key.list_",
}

key_list_failed = {
    "result": False,
    "ret": [],
    "comment": ("some sort of error...",),
    "ref": "exec.aws.iam.access_key.list_",
}


@pytest.fixture
def mock_exec_hub(mock_hub, hub, ctx):
    """provides a hub where everything in hub.exec is mocked"""
    with unittest.mock.patch.object(hub, "exec", new=mock_hub.exec):
        hub.idem.RUNS = {ctx["run_name"]: {}}
        yield hub


async def test_present_test_doesnt_update(mock_exec_hub, ctx):
    hub = mock_exec_hub
    hub.exec.aws.iam.access_key.list.return_value = key_list_1

    # "test" doesn't update
    ctx["test"] = True
    ret = await hub.states.aws.iam.access_key.present(
        ctx,
        name="blah",
        user_name="mock_test_user",
        status="Inactive",
        resource_id="KEYID1",
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.exec.boto3.client.iam.update_access_key.call_count == 0
    ), "Update should not be called with test set"
    assert (
        ret["new_state"]["status"] == "Inactive"
    ), "The new state status should be updated"
    assert (
        hub.exec.aws.iam.access_key.list.call_count == 1
    ), "'test' fakes the action, so list should only be called once"
    del ctx["test"]


async def test_present_name_not_found(mock_exec_hub, ctx):
    hub = mock_exec_hub
    hub.exec.aws.iam.access_key.list.return_value = {
        "result": True,
        "ret": [],
        "comment": [],
        "ref": "exec.aws.iam.access_key.list_",
    }

    ret = await hub.states.aws.iam.access_key.present(
        ctx,
        name="blah",
        user_name="mock_test_user",
        status="Inactive",
        resource_id="KEYID1",
    )
    assert ret["result"] is False, "The call should fail"


async def test_present_cannot_list(mock_exec_hub, ctx):
    hub = mock_exec_hub
    hub.exec.aws.iam.access_key.list.return_value = key_list_failed

    ret = await hub.states.aws.iam.access_key.present(
        ctx,
        name="blah",
        user_name="mock_test_user",
        status="Inactive",
        resource_id="KEYID1",
    )
    assert ret["result"] is False, "The call should fail"
    assert "Error listing access keys" in str(ret["comment"])


async def test_present_cannot_update(mock_exec_hub, ctx):
    hub = mock_exec_hub
    hub.exec.aws.iam.access_key.list.return_value = key_list_1
    hub.exec.aws.iam.access_key.update.return_value = {
        "result": False,
        "comment": ("mock comment",),
    }

    ret = await hub.states.aws.iam.access_key.present(
        ctx,
        name="blah",
        user_name="mock_test_user",
        status="Inactive",
        resource_id="KEYID1",
    )
    assert ret["result"] is False, "Update should have failed"
    # make sure we actually executed the list and update call as expected
    assert hub.exec.aws.iam.access_key.update.call_count == 1
    assert hub.exec.aws.iam.access_key.list.call_count == 1


async def test_present_secret_access_key_persists(mock_exec_hub, ctx):
    hub = mock_exec_hub
    hub.exec.aws.iam.access_key.list.return_value = key_list_1

    ret = await hub.states.aws.iam.access_key.present(
        ctx,
        name="blah",
        user_name="mock_test_user",
        status="Inactive",
        resource_id="KEYID1",
        secret_access_key="SECRETACCESSKEY",
    )
    assert ret["result"], ret["comment"]
    assert "'secret_access_key': 'SECRETACCESSKEY'" in str(ret["new_state"])
