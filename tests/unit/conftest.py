from unittest.mock import AsyncMock
from unittest.mock import create_autospec

import botocore.session
import pop.hub
import pytest
from dict_tools import data
from pop.contract import Contracted


@pytest.fixture(scope="function", name="hub")
def function_hub():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    yield hub


@pytest.fixture(scope="function")
def mock_hub(hub):
    mock_hub = hub.pop.testing.mock_hub()

    # client uses getattr to resolve api calls, make it work
    #  note this doesn't use autospec so parameter validation
    #  must go through the API
    mock_hub.exec.boto3.client = AsyncMock()
    # conversion utils don't have any external dependencies and should just work.
    for contracted in hub.tool.aws.iam.conversion_utils:
        if isinstance(contracted, Contracted):
            setattr(
                mock_hub.tool.aws.iam.conversion_utils, contracted.__name__, contracted
            )

    return mock_hub


@pytest.fixture
def ctx():
    """
    Override pytest-pop's ctx fixture with one that has a mocked session
    """
    yield data.NamespaceDict(
        run_name="test",
        test=False,
        tag="fake_|-test_|-tag",
        acct=data.NamespaceDict(
            provider_tag_key="provider_tag_key",
            session=create_autospec(botocore.session.Session),
            endpoint_url="https://example.com:4566",
        ),
    )
