import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get_and_list(hub, ctx, aws_ec2_security_group):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    ret = await hub.exec.aws.ec2.security_group_rule.list(
        ctx=ctx,
        filters=[
            {
                "name": "group-id",
                "values": [aws_ec2_security_group["resource_id"]],
            }
        ],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert len(ret["ret"]) > 0, ret["comment"]

    rule_id = ret["ret"][0]["resource_id"]
    ret = await hub.exec.aws.ec2.security_group_rule.get(
        ctx,
        resource_id=rule_id,
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert len(ret["ret"]) > 0, ret["comment"]
    resource = ret["ret"]
    assert rule_id == resource.get("resource_id")
