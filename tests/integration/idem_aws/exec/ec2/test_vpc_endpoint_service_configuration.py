"""Tests for validating EC2 VPC Endpoint Service Configurations."""
import uuid

import pytest


@pytest.fixture(scope="module")
def shared_data():
    parameter = {"name": "idem-test-resource-" + str(uuid.uuid4())}
    return parameter


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_create(hub, ctx, shared_data, aws_vpc_endpoint_service_load_balancer_1):
    # Test - create new resource
    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.create(
        ctx,
        name=shared_data["name"],
        acceptance_required=False,
        gateway_load_balancer_arns=[
            aws_vpc_endpoint_service_load_balancer_1.get("resource_id")
        ],
        supported_ip_address_types=["ipv4"],
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    shared_data["resource_id"] = resource.get("resource_id", None)
    assert shared_data["resource_id"]
    assert shared_data["name"] == resource.get("name")

    # Now get the resource
    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.get(
        ctx,
        name=shared_data["name"],
        resource_id=shared_data["resource_id"],
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    assert shared_data["name"] == resource.get("name")
    gateway_load_balancer_arns = resource.get("gateway_load_balancer_arns")
    assert gateway_load_balancer_arns
    assert gateway_load_balancer_arns[
        0
    ] == aws_vpc_endpoint_service_load_balancer_1.get("resource_id")
    assert "tags" in resource


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_get(hub, ctx, shared_data):
    # Test - Invalid/Not-Found resource lookup
    assert shared_data.get(
        "resource_id", None
    ), "The resource might not have been created"

    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.get(
        ctx,
        name=shared_data["name"],
        resource_id="vpce-svc-invalid",
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert "result is empty" in str(ret["comment"])

    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.get(
        ctx,
        name=shared_data["name"],
        resource_id=shared_data["resource_id"],
    )
    assert ret
    assert ret["result"], ret["comment"]
    resource = ret["ret"]
    assert resource
    assert shared_data["resource_id"] == resource.get("resource_id")
    assert shared_data["name"] == resource.get("name")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_list(hub, ctx, shared_data):
    assert shared_data.get(
        "resource_id", None
    ), "The resource might not have been created"

    # Get by service id
    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.list(
        ctx, service_ids=[shared_data["resource_id"]]
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert len(ret["ret"]) == 1
    resource = ret["ret"][0]
    assert resource
    assert shared_data["resource_id"] == resource.get("resource_id")

    if not hub.tool.utils.is_running_localstack(ctx):
        # Get by filter - does not seem to work in localstack
        filters = [
            {
                "Name": "service-name",
                "Values": [
                    resource["service_name"],
                ],
            },
        ]
        ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.list(
            ctx, filters=filters
        )

        assert ret
        assert ret["result"], ret["comment"]
        assert ret["ret"]
        assert len(ret["ret"]) == 1
        resource = ret["ret"][0]
        assert resource
        assert shared_data["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_update(hub, ctx, shared_data, aws_vpc_endpoint_service_load_balancer_2):
    # Test - Update existing resource
    assert shared_data.get(
        "resource_id", None
    ), "The resource might not have been created"

    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.get(
        ctx,
        name=shared_data["name"],
        resource_id=shared_data["resource_id"],
    )
    assert ret
    assert ret["result"], ret["comment"]
    resource = ret["ret"]
    assert resource
    assert resource.get("gateway_load_balancer_arns")
    current_gateway_load_balancer_arns = resource.get("gateway_load_balancer_arns")
    assert current_gateway_load_balancer_arns

    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.update(
        ctx,
        name=shared_data["name"],
        resource_id=shared_data["resource_id"],
        # associate new gateway load balancer and remove old one
        add_gateway_load_balancer_arns=[
            aws_vpc_endpoint_service_load_balancer_2.get("resource_id")
        ],
        remove_gateway_load_balancer_arns=current_gateway_load_balancer_arns,
        tags=[{"Key": "test", "Value": "test_value"}],
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource

    # Now get the resource
    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.get(
        ctx,
        name=shared_data["name"],
        resource_id=shared_data["resource_id"],
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    assert resource.get("gateway_load_balancer_arns")
    updated_gateway_load_balancer_arns = resource.get("gateway_load_balancer_arns")
    assert updated_gateway_load_balancer_arns
    assert updated_gateway_load_balancer_arns[
        0
    ] == aws_vpc_endpoint_service_load_balancer_2.get("resource_id")
    assert "tags" in resource
    assert "test" in resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_delete(hub, ctx, shared_data):
    # Test - Delete existing resource
    assert shared_data.get(
        "resource_id", None
    ), "The resource might not have been created"

    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.delete(
        ctx, name="", resource_id=shared_data["resource_id"]
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert not ret["ret"]

    # Now get the resource
    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.get(
        ctx,
        name=shared_data["name"],
        resource_id=shared_data["resource_id"],
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert "result is empty" in str(ret["comment"])
