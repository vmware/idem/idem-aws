import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_topic(hub, ctx):
    topic_name = "idem-test-topic-" + str(uuid.uuid4())
    delivery_policy = '{"http":{"defaultHealthyRetryPolicy":{"minDelayTarget":10,"maxDelayTarget":30,"numRetries":10,"numMaxDelayRetries":7,"numNoDelayRetries":0,"numMinDelayRetries":3,"backoffFunction":"linear"},"disableSubscriptionOverrides":false}}'
    attributes = {"DeliveryPolicy": delivery_policy}
    tags = [{"Key": "Name", "Value": topic_name}]

    # Create topic with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.sns.topic.present(
        test_ctx, name=topic_name, attributes=attributes, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.sns.topic '{topic_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert topic_name == resource.get("name")
    assert resource.get("attributes")
    resource_attributes = resource.get("attributes")
    for key, value in attributes.items():
        assert key in resource_attributes
        assert value == resource_attributes.get(key)
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )

    # Create topic
    ret = await hub.states.aws.sns.topic.present(
        ctx, name=topic_name, attributes=attributes, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert topic_name == resource.get("name")
    resource_attributes = resource.get("attributes")
    for key, value in attributes.items():
        assert key in resource_attributes
        assert value == resource_attributes.get(key)
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )
    resource_id = resource.get("resource_id")

    # Already present
    ret = await hub.states.aws.sns.topic.present(
        ctx, name=topic_name, resource_id=resource_id, attributes=attributes, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert f"aws.sns.topic '{topic_name}' already exists" in ret["comment"]

    # Describe topics
    describe_ret = await hub.states.aws.sns.topic.describe(ctx)
    assert topic_name in describe_ret
    # Verify that describe output format is correct
    assert "aws.sns.topic.present" in describe_ret.get(topic_name)
    described_resource = describe_ret.get(topic_name).get("aws.sns.topic.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert described_resource_map.get("attributes")
    resource_attributes = described_resource_map.get("attributes")
    for key, value in attributes.items():
        assert key in resource_attributes
        assert value == resource_attributes.get(key)
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags,
        hub.tool.aws.tag_utils.convert_tag_dict_to_list(
            described_resource_map.get("tags")
        ),
    )

    # Updating the attributes with same values, but changing the policy order should result in no_op
    new_delivery_policy = '{"http":{"defaultHealthyRetryPolicy":{"backoffFunction":"linear","maxDelayTarget":30,"numRetries":10,"minDelayTarget":10,"numMaxDelayRetries":7,"numNoDelayRetries":0,"numMinDelayRetries":3},"disableSubscriptionOverrides":false}}'
    new_attributes = {"DeliveryPolicy": new_delivery_policy}
    ret = await hub.states.aws.sns.topic.present(
        ctx,
        name=topic_name,
        resource_id=resource_id,
        attributes=new_attributes,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_attributes = resource.get("attributes")
    for key, value in new_attributes.items():
        assert key in resource_attributes
        assert sorted(value) == sorted(resource_attributes.get(key)) and len(
            value
        ) == len(resource_attributes.get(key))
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )

    # Updating attributes and tags with test flag
    delivery_policy = '{"http":{"defaultHealthyRetryPolicy":{"minDelayTarget":12,"maxDelayTarget":20,"numRetries":5,"numMaxDelayRetries":3,"numNoDelayRetries":0,"numMinDelayRetries":2,"backoffFunction":"linear"},"disableSubscriptionOverrides":false}}'
    attributes = {"DeliveryPolicy": delivery_policy, "DisplayName": "idem-test-topic"}
    tags.append(
        {
            "Key": f"idem-test-topic-key-{str(uuid.uuid4())}",
            "Value": f"idem-test-topic-value-{str(uuid.uuid4())}",
        }
    )

    ret = await hub.states.aws.sns.topic.present(
        test_ctx,
        name=topic_name,
        resource_id=resource_id,
        attributes=attributes,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_attributes = resource.get("attributes")
    for key, value in attributes.items():
        assert key in resource_attributes
        assert value == resource_attributes.get(key)
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )

    # Updating attributes and tags
    ret = await hub.states.aws.sns.topic.present(
        ctx, name=topic_name, resource_id=resource_id, attributes=attributes, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_attributes = resource.get("attributes")
    for key, value in attributes.items():
        assert key in resource_attributes
        assert value == resource_attributes.get(key)
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )

    # Test deleting tags
    tags = [tags[0]]
    ret = await hub.states.aws.sns.topic.present(
        ctx, name=topic_name, resource_id=resource_id, attributes=attributes, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )

    # Delete Topic with test flags
    ret = await hub.states.aws.sns.topic.absent(
        test_ctx, name=topic_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert f"Would delete aws.sns.topic '{topic_name}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete Topic
    ret = await hub.states.aws.sns.topic.absent(
        ctx, name=topic_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert f"Deleted aws.sns.topic '{topic_name}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.sns.topic.absent(
        ctx, name=topic_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert f"aws.sns.topic '{topic_name}' already absent" in ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
