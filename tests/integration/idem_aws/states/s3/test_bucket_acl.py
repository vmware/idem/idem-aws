import copy
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(Pro=False)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, aws_s3_bucket_for_acl):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["name"] = aws_s3_bucket_for_acl["name"]
    result = await hub.exec.boto3.client.s3.get_bucket_acl(
        ctx, Bucket=aws_s3_bucket_for_acl["name"]
    )
    grants = {"Grants": result["ret"]["Grants"], "Owner": result["ret"]["Owner"]}
    PARAMETER["access_control_policy"] = grants
    # Create attack acl policy in real
    ret = await hub.states.aws.s3.bucket_acl.present(ctx, **PARAMETER)
    resource = ret.get("new_state")
    assert resource, ret.get("comment")
    if __test:
        assert f"Would create aws.s3.bucket_acl '{PARAMETER['name']}'" in ret["comment"]
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert f"Created aws.s3.bucket_acl '{PARAMETER['name']}'" in ret["comment"]
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert_acl_policy(hub, ctx, resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(Pro=False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.s3.bucket_acl.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    # Verify that describe output format is correct
    assert "aws.s3.bucket_acl.present" in describe_ret[resource_id + "-bucket_acl"]
    described_resource = describe_ret[resource_id + "-bucket_acl"][
        "aws.s3.bucket_acl.present"
    ]
    described_resource_map = dict(ChainMap(*described_resource))
    assert_acl_policy(hub, ctx, described_resource_map, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(Pro=False)
@pytest.mark.dependency(name="exec-get", depends=["present"])
# This test is here to avoid the need to create an bucket acl fixture for exec.get() testing
async def test_exec_get(hub, ctx):
    ret = await hub.exec.aws.s3.bucket_acl.get(
        ctx=ctx, resource_id=PARAMETER["resource_id"], name=PARAMETER["name"]
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert_acl_policy(hub, ctx, ret["ret"], PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(Pro=False)
@pytest.mark.dependency(name="update_bucket_acl_policy", depends=["describe"])
async def test_update_bucket_acl_policy(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    new_parameter["access_control_policy"]["Grants"][0]["Permission"] = "READ"

    ret = await hub.states.aws.s3.bucket_acl.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.s3.bucket_acl",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type="aws.s3.bucket_acl",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert_acl_policy(hub, ctx, ret["old_state"], PARAMETER)
    assert_acl_policy(hub, ctx, ret["new_state"], new_parameter)
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(Pro=False)
@pytest.mark.dependency(name="absent", depends=["update_bucket_acl_policy"])
async def test_absent(hub, ctx, __test):
    name = PARAMETER["name"]
    absent_message = f"This action cannot be possible as AWS does not provide an API to dissociate the ACL from S3 bucket, The ACL policy will be deleted post '{name}' bucket gets deleted."
    ctx["test"] = __test
    ret = await hub.states.aws.s3.bucket_acl.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert absent_message == ret["comment"][0]
    assert PARAMETER["name"] == ret["name"]


@pytest.mark.asyncio
@pytest.mark.localstack(Pro=False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.s3.bucket_acl.absent(ctx, name=PARAMETER["name"])
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.s3.bucket_acl",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.localstack(Pro=False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="test_already_with_resource_absent", depends=["absent"])
async def test_already_with_resource_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.s3.bucket_acl.absent(
        ctx, name="test", resource_id="test"
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.s3.bucket_acl",
            name="test",
        )[0]
        in ret["comment"]
    )


def assert_acl_policy(hub, ctx, resource, parameters):
    assert parameters.get("name") == resource.get("name")
    assert parameters.get("access_control_policy") == resource.get(
        "access_control_policy"
    )
    assert parameters.get("name") == resource.get("resource_id")
