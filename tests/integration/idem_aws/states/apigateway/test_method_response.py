import copy
import time
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-resource-" + str(int(time.time())),
    "status_code": "400",
    "response_parameters": {"method.response.header.custom-header": True},
}


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, aws_apigateway_method, cleanup):
    global PARAMETER
    rest_api_id = aws_apigateway_method.get("rest_api_id")
    parent_resource_id = aws_apigateway_method.get("parent_resource_id")
    http_method = aws_apigateway_method.get("http_method")

    PARAMETER["rest_api_id"] = rest_api_id
    PARAMETER["parent_resource_id"] = parent_resource_id
    PARAMETER["http_method"] = http_method

    ctx["test"] = __test
    present_ret = await hub.states.aws.apigateway.method_response.present(
        ctx, **PARAMETER
    )
    assert present_ret["result"], present_ret["comment"]
    resource = present_ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.apigateway.method_response", name=PARAMETER["name"]
            )[0]
            in present_ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.apigateway.method_response", name=PARAMETER["name"]
            )[0]
            in present_ret["comment"]
        )
        assert not present_ret.get("old_state") and present_ret.get("new_state")
        resource = present_ret.get("new_state")
        assert PARAMETER["resource_id"] == resource.get("resource_id")
        assert PARAMETER["name"] == resource.get("name")
        assert PARAMETER["rest_api_id"] == resource.get("rest_api_id")
        assert PARAMETER["parent_resource_id"] == resource.get("parent_resource_id")
        assert PARAMETER["http_method"] == resource.get("http_method")
        assert PARAMETER["status_code"] == resource.get("status_code")
        assert PARAMETER["response_parameters"] == resource.get("response_parameters")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.apigateway.method_response.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    assert "aws.apigateway.method_response.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get(
        "aws.apigateway.method_response.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["status_code"] == described_resource_map.get("status_code")
    assert PARAMETER["http_method"] == described_resource_map.get("http_method")
    assert PARAMETER["parent_resource_id"] == described_resource_map.get(
        "parent_resource_id"
    )
    assert PARAMETER["rest_api_id"] == described_resource_map.get("rest_api_id")
    assert PARAMETER["resource_id"] == described_resource_map.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get", depends=["present"])
async def test_exec_get(hub, ctx):
    ret = await hub.exec.aws.apigateway.method_response.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert PARAMETER["rest_api_id"] == resource.get("rest_api_id")
    assert PARAMETER["parent_resource_id"] == resource.get("parent_resource_id")
    assert PARAMETER["http_method"] == resource.get("http_method")
    assert PARAMETER["status_code"] == resource.get("status_code")


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get-invalid-resource-id", depends=["present"])
async def test_get_invalid_resource_id(hub, ctx):
    resource_id = "fake-id"
    ret = await hub.exec.aws.apigateway.method_response.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=resource_id,
    )
    assert ret["result"] is False, ret["comment"]
    assert ret["ret"] is None
    assert f"Invalid Resource ID '{resource_id}'." in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get-not-found", depends=["present"])
async def test_get_resource_id_does_not_exist(hub, ctx):
    resource_id = "restApiId-resourceId-httpMethod-100"
    ret = await hub.exec.aws.apigateway.method_response.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get '{resource_id}' result is empty" and f"NotFoundException" in str(
        ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="update", depends=["describe"])
async def test_update(hub, ctx, __test):
    ctx["test"] = __test
    new_parameter = copy.deepcopy(PARAMETER)
    update_response_parameters = {"method.response.header.custom-header": False}
    new_parameter["response_parameters"] = update_response_parameters
    ret = await hub.states.aws.apigateway.method_response.present(ctx, **new_parameter)

    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.apigateway.method_response",
                name=new_parameter["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert ret["result"], ret["comment"]
        assert ret.get("old_state") and ret.get("new_state")
        resource = ret["new_state"]
        assert new_parameter["name"] == resource["name"]
        assert new_parameter["resource_id"] == resource["resource_id"]
        assert new_parameter["rest_api_id"] == resource["rest_api_id"]
        assert new_parameter["parent_resource_id"] == resource["parent_resource_id"]
        assert new_parameter["http_method"] == resource["http_method"]
        assert new_parameter["status_code"] == resource["status_code"]
        assert new_parameter["response_parameters"] == resource["response_parameters"]
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type="aws.apigateway.method_response",
                name=new_parameter["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="absent", depends=["update"])
async def test_absent_method_response(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test

    ret = await hub.states.aws.apigateway.method_response.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert PARAMETER["rest_api_id"] == resource.get("rest_api_id")
    assert PARAMETER["parent_resource_id"] == resource.get("parent_resource_id")
    assert PARAMETER["http_method"] == resource.get("http_method")
    assert PARAMETER["status_code"] == resource.get("status_code")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.apigateway.method_response",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.apigateway.method_response",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.apigateway.method_response.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigateway.method_response",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_method_response_absent_with_none_resource_id(hub, ctx):
    "idem-method-response-" + str(int(time.time()))
    # Delete method response with resource_id as None. Result in no-op.
    ret = await hub.states.aws.apigateway.method_response.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=None,
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigateway.method_response",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )


@pytest.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.apigateway.method_response.absent(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
        )
        assert ret["result"], ret["comment"]
