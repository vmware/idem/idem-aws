Python Pip
==========

Idem can be pip installed into an existing Python installation.
Python 3.8 is the minimum version supported by Idem.

Python 3.8 or greater required
++++++++++++++++++++++++++++++

Verify you have Python 3.8 or later installed:

.. code-block:: bash

    python -V

Install Idem
++++++++++++

.. note::

    You might need to use `pip3` in the commands below.
    Use `pip3` if the output of the following command points to Python 2.x.

    ``pip --version``

    ``pip xx.x.x from /usr/lib/python3/dist-packages/pip (python 3.x)``


Once you've verified you have a supported version of Python run the following
to install Idem.

.. code-block:: bash

    pip install idem


Check your version of Idem.

.. code-block:: bash

    idem --version


Now install `idem-aws` like this:


.. code-block:: bash

   pip install idem-aws

.. include:: ./_includes/install_idem-aws.rst
